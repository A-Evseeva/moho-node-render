from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='mohonoderender',
    version="1.2",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    include_package_data=True,
    install_requires=[
        'ijson>=3.1.3',
        'pyqt5>=5.15.2',
        'configparser>=5.0.1',
        'pillow>=8.0.1'
    ],
    entry_points={
    'console_scripts':
        ['mohonoderender = mohonoderender.render:run_render_app']
    }
)