import zipfile, sys, os, time, subprocess, re, tempfile
from shutil import copyfile, rmtree

from threading import Timer,Thread,Event
import winsound

import ijson
import ijson.backends.yajl2_c as IJSON

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import (QApplication, QDialog,
                             QProgressBar, QPushButton, QLabel, QVBoxLayout)

import configparser

from PIL import Image

import http.server
from http import HTTPStatus
import webbrowser, multiprocessing, time

class HttpProcessor(http.server.SimpleHTTPRequestHandler):
    def send_head(self):
        path = HttpProcessor.returnFilePath
        f = open(path, 'rb')
        ctype = self.guess_type(path)
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-type", ctype)
        self.end_headers()
        return f

    def log_message(self, format, *args):
        if sys.stderr:
            super().log_message(format, *args)

def run_webserver(path, port=8666):
    server_address = ('127.0.0.1', port)
    HttpProcessor.returnFilePath = path
    serv = http.server.HTTPServer(server_address,HttpProcessor)
    serv.serve_forever()

def displayFile(path):
    p = multiprocessing.Process(target=run_webserver, args=(path, 8666))
    p.daemon = True
    p.start()
    time.sleep(1)
    webbrowser.open("http://127.0.0.1:8666")
    time.sleep(10)
    p.terminate()
    p.join()        

class perpetualTimer():

    def init(self,t,hFunction):
        self.t=t
        self.hFunction = hFunction
        self.thread = Timer(self.t,self.handle_function)

    def handle_function(self):
        self.hFunction()
        self.thread = Timer(self.t,self.handle_function)
        self.thread.start()

    def start(self):
        self.thread.start()

    def cancel(self):
        self.thread.cancel()

class ProgressCounter():
    def set(self, path, number, interval, printMethod):
        self.path = path
        self.number = number
        self.interval = interval
        self.startTime = time.time()
        self.printMethod = printMethod

    def tick(self):
        curtime = time.time() - self.startTime
        numfiles = 0
        for filename in os.listdir(self.path):
            if os.path.getmtime(os.path.join(self.path, filename)) > self.startTime:
                numfiles += 1
        if numfiles >= self.number: self.timer.cancel()
        if numfiles == 0: return
        progress = numfiles/self.number
        total = curtime/progress
        estimated = total - curtime
        text = 'Passed {0}:{1:02d}'.format(int(curtime/60), int(curtime%60))
        if numfiles < self.number:
            text = text + ', estimated ~ {0}:{1:02d}'.format(int(estimated/60), int(estimated%60))
        self.printMethod(text, end='', toReplace=True)

    def run(self):

        self.timer = perpetualTimer()
        self.timer.init(self.interval, self.tick)
        self.timer.start()

    def stop(self):
        self.timer.cancel()
        total = time.time() - self.startTime
        text = 'Passed {0}:{1:02d}'.format(int(total / 60), int(total % 60))
        self.printMethod(text, end='', toReplace=True)

class External(QThread):

    """
    Runs render thread.
    """
    addLine = pyqtSignal(str)
    replaceLine = pyqtSignal(str)

    def print(self, *texts, end='\n', toReplace = False):
        text = ''
        for t in texts: text = text + t
        if toReplace: self.replaceLine.emit(text + end)
        else: self.addLine.emit(text + end)

    def getIniFilePath(self):
        myDir = os.path.dirname(__file__)
        return os.path.join(myDir, 'node_render.ini')

    def savePrefs(self):
        config = configparser.ConfigParser()
        config['DEFAULT'] = {'MohoPath': self.mohoPath, 'FFMPEG': self.FFMPEG}
        with open(self.getIniFilePath(), 'w') as configfile:
            config.write(configfile)


    def readPrefs(self):
        config = configparser.ConfigParser()
        config.read(self.getIniFilePath())
        if 'MohoPath' in config['DEFAULT']:
            self.mohoPath = config['DEFAULT']['MohoPath']
        else: self.mohoPath = False
        if 'FFMPEG' in config['DEFAULT']:
            self.FFMPEG = config['DEFAULT']['FFMPEG']
        else: self.FFMPEG = "C:\\ffmpeg.exe"

        #TODO: (optional) if no .ini file, ask user for moho path

    def parseMohoproj(self):
        self.print(self.projectFile)
        archive = zipfile.ZipFile(self.projectFile, "r", allowZip64=True)
        mohoprojfile = archive.read("Project.mohoproj")
        archive.close()

        parser = IJSON.parse(mohoprojfile)

        self.soundfile = None
        self.start_frame = 1
        self.end_frame = 1
        self.width = 0
        self.height = 0
        self.layers = []

        layer = None

        for prefix, event, value in parser:
            if not layer:
                if prefix == 'project_data.end_frame': self.end_frame = value
                if prefix == 'project_data.start_frame': self.start_frame = value
                if prefix == 'project_data.width': self.width = value        
                if prefix == 'project_data.height': self.height = value 
            if (prefix, event) == ('layers.item', 'start_map'): 
                layer = {}
            if (prefix, event) == ('layers.item', 'end_map'): 
                    self.layers.append(layer)
                    layer = None
            if prefix == 'layers.item.image_path':
                if value.endswith('.mp4'):
                    self.soundfile = value
            if prefix == 'layers.item.audio_path':
                if value.endswith('.wav'):
                    self.soundfile = value            
            if prefix == 'layers.item.text':
                layer['text'] = value
            if layer:                
                if prefix == 'layers.item.layer_effects.visibility.type':
                    layer['val'] = []
                    layer['when'] = []
                    layer['interp'] = []
                if (prefix, event) == ('layers.item.layer_effects.visibility.when.item','number'):
                    layer['when'].append(value)
                if (prefix, event) == ('layers.item.layer_effects.visibility.interp.item.im','number'):
                    layer['interp'].append(value) 
                if (prefix, event) == ('layers.item.layer_effects.visibility.val.item','boolean'):
                    layer['val'].append(value)                               

    def getKeyIndex(self, layerInfo, frame):
        prevKeyIndex = len(layerInfo['when']) - 1
        while prevKeyIndex >= 0 and layerInfo['when'][prevKeyIndex] > frame:
            prevKeyIndex -= 1  
        return prevKeyIndex  

    def prepareFolder(self, folderName):
        if os.path.exists(folderName): rmtree(folderName)
        try: os.makedirs(folderName)       
        except PermissionError: 
            self.print('Folder {0} is busy. Free the folder (close apps using it) and try again.'.format(folderName))
            self.print('Now render will be closed')
            self.terminate()
            return        

    def evalMohoRender(self, layerInfo, toKillFolder):
        if not 'text' in layerInfo: return
        layercompName = layerInfo['text']
        dirName = self.tempDir
        if self.outputMode == 'sequence': dirName = self.outputFile
        subDirName = os.path.join(dirName, '{0}-{1}'.format(self.filename, layercompName))
        layerInfo['subDirName'] = subDirName
        if toKillFolder: self.prepareFolder(subDirName)
        intervals = []
        if self.outputMode == 'frame':
            prevKeyIndex = self.getKeyIndex(layerInfo, self.start_frame)
            val = layerInfo['val'][prevKeyIndex]
            if (val):
                intervals.append({'start':self.start_frame, 'end':self.end_frame})
        else:
            index = 0
            length = len(layerInfo['when'])
            while index < length:
                while index < length and not layerInfo['val'][index]: index += 1
                if index < length and layerInfo['val'][index]:
                    start = layerInfo['when'][index]
                    end = (index + 1 >= length) and self.end_frame or layerInfo['when'][index + 1] - 1
                    if layerInfo['interp'][index] == 3: end = start
                    if end >= self.start_frame and start <= self.end_frame:
                        if start < self.start_frame: start = self.start_frame
                        if end > self.end_frame: end = self.end_frame
                        intervals.append({'start':start, 'end':end})
                    index += 1
        for interval in intervals:
            renderstring =  '"{0}" -r "{1}" -o "{2}" -layercomp "{3}" -f PNG -start {4} -end {5}'.format(
                self.mohoPath, self.projectFile, subDirName, layercompName, interval['start'], interval['end']) 
            self.print('rendering {0} from {1} to {2} ... '.format(layercompName, interval['start'], interval['end']))
            progressThread = ProgressCounter()
            progressThread.set(subDirName, interval['end'] - interval['start'], 5, self.print)
            progressThread.run()
            subprocess.check_call(renderstring)
            progressThread.stop()
            self.print(' finished')


    def getFrameFilename(self, frame):
        return '{0}_{1:05d}.png'.format(self.filename, frame)

    def getLayerFrameFilename(self, layerInfo, frame):
        if not 'subDirName' in layerInfo: return None
        return os.path.join(layerInfo['subDirName'], self.getFrameFilename(frame))

    def fillBlankFrames(self, layerInfo):
        if not 'subDirName' in layerInfo: return
        length = len(layerInfo['when'])
        for index in range(length):
            if layerInfo['val'][index] == False:
                lastFrame = self.end_frame + 1
                if index < (length-1) and layerInfo['when'][index + 1] < lastFrame: 
                    lastFrame = layerInfo['when'][index + 1] 
                if lastFrame > self.start_frame:
                    firstFrame = layerInfo['when'][index]
                    if firstFrame < self.start_frame: firstFrame = self.start_frame
                    self.print('filling blanks for {0} from {1} to {2} ...'.format(layerInfo['text'], firstFrame, lastFrame-1), end='')
                    for frame in range(firstFrame, lastFrame):
                        blankName = self.getLayerFrameFilename(layerInfo, frame)
                        self.blankImage.save(blankName, 'PNG')
                    self.print('finished')

    def composeLayers(self):
        self.print('composing from {0} to {1} ...'.format(self.start_frame, self.end_frame), end='')
        if self.outputMode == 'movie':
            self.composeDir = os.path.join(self.tempDir, self.filename + '.compose')
            self.prepareFolder(self.composeDir)
        for frame in range(self.start_frame, self.end_frame + 1):
            bg = False
            for layer in self.layers:
                if 'subDirName' in layer:
                    prevKeyIndex = self.getKeyIndex(layer, frame)
                    if layer['val'][prevKeyIndex]:
                        filename = self.getLayerFrameFilename(layer, frame)
                        if not os.path.exists(filename) and self.outputMode != 'frame':
                            if prevKeyIndex < 0:
                                self.print('something went wrong on frame {0} layer {1}'.format(frame, layerInfo['text']))
                                return False
                            if layer['val'][prevKeyIndex]:
                                if layer['interp'][prevKeyIndex] == 3:
                                    otherframe = layer['when'][prevKeyIndex]
                                    if otherframe < self.start_frame: otherframe = self.start_frame
                                    filename = self.getLayerFrameFilename(layer, otherframe)
                                else:
                                    self.print('not found ' + filename)
                                    return False
                        if os.path.exists(filename):
                            if not bg: bg = Image.open(filename)
                            else:
                                ol = Image.open(filename)
                                bg.alpha_composite(ol)
            saveFilename = self.outputFile
            #if self.outputMode == 'frame' and saveFilename[0] == "*": saveFilename = os.path.join(self.tempDir, '{0}_{1:05d}.png'.format(self.filename, self.start_frame))
            if self.outputMode == 'movie': 
                saveFilename = os.path.join(self.composeDir, self.getFrameFilename(frame))
            if os.path.exists(saveFilename): os.remove(saveFilename)
            resultImage = bg
            if not bg: resultImage = self.blankImage
            resultImage.save(saveFilename)
            if self.outputMode == 'frame': 
                self.print('FINISHED')
                self.print('image saved to ' + saveFilename)
                #resultImage.show()
                displayFile(saveFilename)

        self.print('finished')
        if self.outputMode == 'frame':
            sys.exit(0)

        return True

    def createMovie(self):
        if not os.path.exists(self.FFMPEG):
            self.print('Can not create movie: FFMPEG not found at {0}. Please edit {1} manualy'.format(self.FFMPEG, self.getIniFilePath()))
            return
        self.print('Creating {0} ...'.format(self.outputFile), end='')
        composeSequence = os.path.join(self.composeDir, '{0}_%05d.png'.format(self.filename)) 
        movieString = '"{0}" -y -r 25 -i "{1}" -r 25 -pix_fmt yuv420p "{2}"'.format(self.FFMPEG, composeSequence, self.outputFile)
        if self.soundfile:
            movieString = '"{0}" -y -r 25 -i "{1}" -i "{2}" -map 0:v:0 -map 1:a:0 -shortest -r 25 -pix_fmt yuv420p "{3}"'.format(self.FFMPEG, composeSequence, self.soundfile, self.outputFile)
        subprocess.check_call(movieString)
        self.print(" ...FINISHED!") 
        displayFile(self.outputFile) 
        sys.exit(0)       

    def run(self):
        if len(sys.argv)<3: 
            self.print("Not enouth args: {0} instead of 2 or 3".format(len(sys.argv)-1))
            return

        self.projectFile = sys.argv[1]
        self.outputFile = sys.argv[2]
        self.filepath, self.filename = os.path.split(self.projectFile)
        self.filename = self.filename[:-5]
        
        self.readPrefs()        
        if len(sys.argv) > 3:
            self.mohoPath = sys.argv[3]
            self.savePrefs()
        else:
            if not self.mohoPath:
                self.print("Can not render without path to Moho executive")
                return

        outputExtension = self.outputFile.split('.')[-1]
        if outputExtension == 'png': self.outputMode = 'frame'
        elif outputExtension == 'mp4': self.outputMode = 'movie'
        else: self.outputMode = 'sequence'

        self.parseMohoproj()  

        self.blankImage = Image.new('RGBA', (self.width, self.height), (0,0,0,0))    

        if self.outputMode == 'frame':
            filename = self.outputFile.split('.')[-2]
            numberString = filename.split('_')[-1]
            if numberString:
                number = int(numberString)
                if number:
                    self.start_frame = number
                    self.end_frame = number     

        self.tempDir = tempfile.gettempdir()

        allwaysRenderedNames = []
        for layer in self.layers:
            toKillFolder = not ('text' in layer and layer['text'] in allwaysRenderedNames)
            self.evalMohoRender(layer, toKillFolder)
            if 'text' in layer: allwaysRenderedNames.append(layer['text'])

        result = True
        if self.outputMode == 'sequence':
            for layer in self.layers: self.fillBlankFrames(layer)
        else:
            result = self.composeLayers()

        if result and self.outputMode == 'movie': self.createMovie()


class Actions(QDialog):
    """
    Simple dialog that consists of a Progress Bar and a Button.
    Clicking on the button results in the start of a timer and
    updates the progress bar.
    """
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle('Render')
        self.label = QLabel('Starting...\n', self)

        layout = QVBoxLayout() 
        layout.addWidget(self.label)
        self.setLayout(layout)
        self.show()

        self.renderThread = External()
        self.renderThread.addLine.connect(self.onAddLine)
        self.renderThread.replaceLine.connect(self.onReplaceLine)
        self.renderThread.start()

    def onAddLine(self, value):
        self.label.setText(self.label.text() + value)

    def onReplaceLine(self, value):
        oldText = self.label.text().split('\n')
        oldText[-1] = value
        newText = "\n".join(oldText)
        self.label.setText(newText)




def run_render_app():
    app = QApplication(sys.argv)
    window = Actions()
    sys.exit(app.exec_())

if __name__ == '__main__': run_render_app()