-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "AE_Node_Render"

-- **************************************************
-- General information about this script
-- **************************************************

AE_Node_Render = {}

function AE_Node_Render:Name()
	return 'Node Render'
end

function AE_Node_Render:Version()
	return '1.0'
end

function AE_Node_Render:UILabel()
	return 'Node Render'
end

function AE_Node_Render:Creator()
	return 'Alexandra Evseeva'
end

function AE_Node_Render:Description()
	return ''
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function AE_Node_Render:IsRelevant(moho)
	return true
end

function AE_Node_Render:IsEnabled(moho)
	return true
end

-- **************************************************
-- AE_Node_RenderDialog
-- **************************************************

local AE_Node_RenderDialog = {}

function AE_Node_RenderDialog:new()
	local d = LM.GUI.SimpleDialog('Node Render', AE_Node_RenderDialog)
	local l = d:GetLayout()
		
	d.button2Button = LM.GUI.ImageButton(AE_Node_Render.image)
    l:AddChild(d.button2Button)	
		
	return d
end

-- **************************************************
-- The guts of this script
-- **************************************************

function AE_Node_Render:Run(moho)
	--[[
	local dlog = AE_Node_RenderDialog:new(moho)
	if (dlog:DoModal() == LM.GUI.MSG_CANCEL) then
		return
	end
	--]]
	
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)
	
	-- Your code here:
	local myself = debug.getinfo(1).source:sub(2)
	--local exescript = myself:gsub('.lua', '.exe')
	local exescript = myself:gsub('.lua', '.py')
	local mohopath = moho:ExporterPath()
	local projectpath = moho.document:Path()
	local projectname = string.sub(moho.document:Name(), 1, -6)
	
	local s,e = string.find(myself, "Moho Pro")
	--local s,e = string.find(myself, "scripts")
	local outputFrameName = string.format("%s_%05d.png", projectname, moho.frame)
	--local outputFrameName = string.format("%s_%05d.png", "noderender", moho.frame)
	outputFileName = string.sub(myself, 1, e+1) .. "Render Cache" .. string.sub(myself, e+1, e+1) .. outputFrameName
	--outputFileName = string.sub(myself, 1, e+1) .. "ScriptResources" .. string.sub(myself, e+1, e+1) .. outputFrameName
	local userSelectedFile = LM.GUI.SelectFolder("Select folder to render movie or cancel to render frame")
	if #userSelectedFile > 0 then
		outputFileName = userSelectedFile .. string.sub(myself, e+1, e+1) .. projectname .. ".mp4" 
	end
	
	local runCommand = string.format( 'mohonoderender "%s" "%s" "%s"', projectpath, outputFileName, mohopath)
	runCommand = string.gsub(runCommand, "\\", "/")
	print(runCommand)
	if  #userSelectedFile > 0 then io.popen(runCommand)
	else os.execute(runCommand) end
--[[	
	if self:file_exists(outputFileName) then
		if #userSelectedFile == 0 then
			print("image")
			local scriptDir = string.sub(myself, 1, e+1) .. "scripts" .. string.sub(myself, e+1, e+1) .. "ScriptResources" .. string.sub(myself, e+1, e+1)
			os.rename(outputFileName, (scriptDir .. "noderender.png")
			--self.image = string.sub(string.gsub(string.sub(outputFileName, e+2), "\\", "/"), 1, -5)
			self.image = "ScriptResources/noderender"
			local dlog = AE_Node_RenderDialog:new(moho)
			if (dlog:DoModal() == LM.GUI.MSG_CANCEL) then
				return
			end
		else 
			print("movie")
		end
	end
--]]	

end

function AE_Node_Render:file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true 
   else 
		print(name, " does not exist")
		return false 
   end
end
